import React, { Component } from 'react';
import Header from './header';
import Slider from './slider';
import Listproduct from './listproduct';
import Footer from './footer';
class Home extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Slider/>
                <section className="bg-dark py-5">
                    <Listproduct/>
                </section>
                
                <section className="bg-light py-5">
                    <Listproduct/>
                </section>
                <section className="bg-dark p-4">
                    <Footer/>
                </section>
               
                
            </div>
        );
    }
}

export default Home;