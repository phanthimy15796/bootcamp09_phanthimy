import React, { Component } from "react";
import product1 from "./img/sp_iphoneX.png";
import product2 from "./img/sp_note7.png";
import product3 from "./img/sp_vivo850.png";
import product4 from "./img/sp_blackberry.png";
class Itemproduct extends Component {
  render() {
    return (
      <div className="card bg-light" style={{ width: 300 }}>
        <img
          className="card-img-top"
          src={product1}
          alt="Card image"
          style={{ maxWidth: "100%", height: 250 }}
        />
        <div className="card-body text-center">
          <h4 className="card-title text-center">iPhone X</h4>
          <p className="card-text">
            iPhone X features a new all-screen design. Face ID, which makes your
            face your password
          </p>
          <a href="#" className="btn btn-primary">
            Detail
          </a>
          <a href="#" className="btn btn-danger">
            Cart
          </a>
        </div>
      </div>
    );
  }
}

export default Itemproduct;
