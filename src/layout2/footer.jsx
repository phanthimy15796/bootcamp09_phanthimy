import React, { Component } from "react";
import Imgfooter from "./imgfooter";
class Footer extends Component {
  render() {
    return (
      <section className="container">
        <h1 className="text-center mb-4 text-white">PROMOTION</h1>
        <div className="d-flex">
          <Imgfooter />
          <Imgfooter />
          <Imgfooter />
        </div>
      </section>
    );
  }
}

export default Footer;
